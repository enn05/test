import React from 'react';
import './App.css';
import TodoForm from './components/Page1-todoForm/TodoForm';
import Data from './components/Page3-displayData/Data';
import TodoList from './components/Page2-todoList/TodoList';
import TodoContextProvider from './context/TodoContext';
import { 
  Switch,
  Route,
  BrowserRouter as Router,
} from 'react-router-dom';
import Particles from 'react-particles-js'

const particlesOption = {
  particles: {
    number: {
     value: 50,
     density: {
        enable: true,
        value_area: 300
     }
    },
  }
}

function App() {
  return (
    <Router>
      <Switch>
        <TodoContextProvider>
          <Particles className='particles'
            params={particlesOption}
          />
          <Route exact path='/' component={TodoForm} />
          <Route exact path='/list' component={TodoList} />
          <Route exact path='/data' component={Data} />
        </TodoContextProvider>
      </Switch>
    </Router>
  );
}

export default App;
