import React, { useContext, useState } from 'react'
import Navbar from '../Layout/Navbar';
import FormInput from './FormInput';
import { TodoContext } from '../../context/TodoContext';

const TodoForm = () => {
  const {addTodo, todos} = useContext(TodoContext)

  const [title, setTitle] = useState('')
  const [details, setDetails] = useState('')

  const refresh = () => {
    setTitle('')
    setDetails('')
  }

  const handleTitleChange = e => {
    if(e.target.value === ''){
      setTitle('')
    } else {
      setTitle(e.target.value)
    }
  }

  const hanldeDetailsChange = e => {
    if(e.target.value === ''){
      setDetails('')
    } else {
      setDetails(e.target.value)
    }
  }

  const handleSave = () => {
    addTodo(title, details)
    refresh()
  }

  return (
    <React.Fragment>
      <Navbar />
      <h3 
        style={{
          textAlign: "center",
          fontSize: "2em",
          color: "#c4dfe6",
          textShadow: "2px 2px #021c1e"
        }}
      >Todo Form</h3>
      <div style={{
        padding: "20px",
        margin: "0 20%",
        boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)",
        background: "linear-gradient(to right, #003b46, #07575b)"
      }}>
        <FormInput
          label={"Title"}
          name={"title"}
          type={"text"}
          placeholder={"Enter Title"}
          onChange={handleTitleChange}
          value={title}
        />
        <label
          style={{
            color: "#c4dfe6"
          }}
        >Details</label>
          <textarea
          style={{
            width: "100%",
            height: "150px",
            padding: "12px 20px",
            margin: "8px 0",
            boxSizing: "border-box",
            border: "1px solid #c4dfe6",
            resize: "none",
          }}
          placeholder={"Details..."}
          value={details}
          onChange={hanldeDetailsChange}
          ></textarea>
        <button
          style={{
            width: "100%",
            padding: "14px 20px",
            margin: "8px 0",
            border: "none",
            cursor: "pointer",
            background: "#021c1e",
            color: "#c4dfe6"
          }}
          onClick={handleSave}
        >Add Task</button>
      </div>
    </React.Fragment>
  )
}

export default TodoForm;