import React, {useContext} from 'react'
import './todolist.css'
import { TodoContext } from '../../context/TodoContext';

const TodoCard = ({todo}) => {
  const { deleteTodo } = useContext(TodoContext)
  return(
    <React.Fragment>
      <div className="todo-card">
        <div className="todo-card-body">
          <h3>{todo.title}</h3>
          <div className="todo-card-details">
            <time>{todo.date}</time>
            <p>{todo.details}</p>
          </div>
          <button 
            className="todo-card-btn"
            onClick={() => deleteTodo(todo.id)}
          >Delete Task</button>
        </div>
      </div>
    </React.Fragment>
  )
}

export default TodoCard