import React from 'react'
import {
  Link,
} from 'react-router-dom';
import './navbar.css'

const Navbar = () => {
  return (
    <React.Fragment>
      <nav>
        <ul>
          <li>
            <Link to='/'>Add Todo</Link>
          </li>
          <li>
            <Link to='/list'>Todo List</Link>
          </li>
          <li>
            <Link to='/data'>Data</Link>
          </li>
        </ul>
      </nav>
    </React.Fragment>
  )
}

export default Navbar
