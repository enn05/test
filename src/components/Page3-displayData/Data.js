import React, { useState, useEffect } from "react";
import { Table } from "reactstrap";
import Navbar from "../Layout/Navbar";
import DataRow from "./DataRow";

const Data = props => {
  const [datas, setDatas] = useState([]);

  useEffect(() => {
    fetch("https://dummy.restapiexample.com/api/v1/employees")
      .then(res => res.json())
      .then(data => {
        setDatas(data.data);
      });
  }, []);

  return (
    <React.Fragment>
      <Navbar />
      <h3
        style={{
          textAlign: "center",
          fontSize: "2em",
          color: "#c4dfe6",
          textShadow: "2px 2px #021c1e"
        }}
      >
        Employees Data
      </h3>
      <div className="container">
        <Table bordered dark>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Salary</th>
              <th>Age</th>
            </tr>
          </thead>
          <tbody>
            {datas.map(data => (
              <DataRow 
                key={data.id} 
                data={data} 
              />
            ))}
          </tbody>
        </Table>
      </div>
    </React.Fragment>
  );
};

export default Data;
