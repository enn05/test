import React, { createContext, useState } from 'react'
import moment from 'moment'
import Swal from 'sweetalert2';

export const TodoContext = createContext();

const TodoContextProvider = (props) => {
  const [todos, setTodos] = useState([
    {
      id: 1,
      title: 'First Task',
      details: 'This is the first task on out todo app',
      date: '2/15/2020'
    },
    {
      id: 2,
      title: 'Second Task',
      details: 'This is the second task on out todo app',
      date: '2/15/2020'
    }
  ])

  const addTodo = (title, details) => {
    if(!title || !details){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Title and Details are required!',
      })
    }else{
      let id = moment(new Date()).format('x')
      let date = moment(new Date()).format("MM/DD/YYYY")
      let newTodos = [...todos]
      newTodos.push({id, title, details, date})
      setTodos(newTodos)
      Swal.fire(
        'Success!',
        'New task added.',
        'success'
      )
    }
  }

  const deleteTodo = (todoId) => {
    
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
        let newTodos = todos.filter(todo => todo.id !== todoId)
        setTodos(newTodos)
      }
    })
  }

  return(
    <TodoContext.Provider value={{todos, addTodo, deleteTodo}}>
      {props.children}
    </TodoContext.Provider>
  )
}

export default TodoContextProvider