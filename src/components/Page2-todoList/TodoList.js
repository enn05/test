import React, { useContext } from 'react'
import Navbar from '../Layout/Navbar';
import { TodoContext } from '../../context/TodoContext';
import TodoCard from './TodoCard';
import './todolist.css'

const TodoList = () => {
  const {todos} = useContext(TodoContext)

  return (
    <React.Fragment>
        <Navbar />
        <h3 className="todo-list-title">Todo List</h3>
        <div className="card-list">
          {todos.map(todo =>
            <TodoCard 
            key={todo.id}
            todo={todo}
            />  
            )}
        </div>
    </React.Fragment>
  )
}

export default TodoList
