import React from 'react'

const FormInput = ({ label, name, type, placeholder, onChange, value }) => {
  return (
    <React.Fragment>
      <div>
        <label 
          style={{
            color: "#c4dfe6"
          }}
        >{label}</label>
        <input
          name={name}
          type={type}
          placeholder={placeholder}
          style={{
            width: "100%",
            padding: "12px 20px",
            margin: "8px 0",
            display: "inline-block",
            border: "1px solid #c4dfe6",
            boxSizing: "border-box",
          }}
          onChange={onChange}
          value={value}
        />
      </div>
    </React.Fragment>
  )
}

export default FormInput
