import React from 'react'

const DataRow = (props) => {
  const data = props.data
  return (
    <React.Fragment>
      <tr>
        <td>{data.id}</td>
        <td>{data.employee_name}</td>
        <td>{data.employee_salary}</td>
        <td>{data.employee_age}</td>
      </tr>
    </React.Fragment>
  )
}

export default DataRow
